#include "csapp.h"
#include "md5.h"
#include <getopt.h>
#include <sys/resource.h>

#define SIZE_MD5_STR 32

void hash(int connfd);
void md5_bin2str(char *, md5_hash_t);
void *thread(void *vargp);


//Variables globales que determinan si una opción esta siendo usada
int nflag = 0; //Opción -n, ignora los saltos de línea
int pflag = 0; //Opción -p, especifica el puerto
int dflag = 0; //Opcion -d, crear daemon

int main(int argc, char **argv)
{
	int fd0, fd1, fd2;
	int listenfd, connfd, *connfdp;
	unsigned int clientlen;
	struct sockaddr_in clientaddr;
	struct hostent *hp;
	char *haddrp, *port;
	int c;
	pid_t pid;
	struct rlimit rl;
	pthread_t tid;

	while ((c = getopt (argc, argv, "ndp:")) != -1)
	{
		switch(c)
		{
			case 'n':
				nflag = 1;
				break;
			case 'p':
				pflag = 1;
				port = optarg;
				break;

			case 'd':
				dflag = 1;
				break;

			case '?':
			default:
				fprintf(stderr, "uso: %s [-n] [-d] -p <port>\n", argv[0]);
				return -1;
		}
	}

	//Opción -p es obligatoria
	if(!pflag)
	{
		fprintf(stderr, "uso: %s [-n] [-d] -p <port>\n", argv[0]);
		return -1;
	
	}
	if (dflag)
	{
		if ((pid = fork()) < 0)
			fprintf(stderr, "fork erroneo %s\n", argv[0]);
		else if (pid != 0)
			exit(0);
		setsid();

		if (rl.rlim_max == RLIM_INFINITY)
			rl.rlim_max = 1024;
		for(int i = 0; i < rl.rlim_max; i++)
			close(i);

		close(0);
		close(1);
		close(2);
		fd0 = open("/dev/null", O_RDWR);
		fd1=dup(0);
		fd2 = dup(0);
	}

	listenfd = Open_listenfd(port);
	while (1) {
		clientlen = sizeof(clientaddr);
		/*

		 Determine the domain name and IP address of the client 
		hp = Gethostbyaddr((const char *)&clientaddr.sin_addr.s_addr,
					sizeof(clientaddr.sin_addr.s_addr), AF_INET);
		haddrp = inet_ntoa(clientaddr.sin_addr);
		printf("server connected to %s (%s)\n", hp->h_name, haddrp);*/
		
		connfdp = Malloc(sizeof(int));
		*connfdp = Accept(listenfd, (SA *) &clientaddr, &clientlen);
		Pthread_create(&tid, NULL, thread, connfdp);
		//hash(connfd);
		//Close(connfd);
	}
	exit(0);
}

//Procesa los datos enviados por el cliente
void hash(int connfd)
{
	size_t n;
	char buf[MAXLINE];
	char md5_str[SIZE_MD5_STR + 1]; //Agrega un caracter extra para '\n'
	rio_t rio;
	md5_hash_t h;

	Rio_readinitb(&rio, connfd);
	while((n = Rio_readlineb(&rio, buf, MAXLINE)) != 0) {
		printf("hashing: %s", buf);

		//Ignora '\n' asumiendo que esta al final de buf
		if(nflag)
			n--;

		h = md5((uint8_t *) buf, n); //Convierte a MD5
		md5_bin2str(md5_str, h); //Convierte el hash MD5 a cadena de caracteres
		printf("Hash: %s", md5_str);
		Rio_writen(connfd, md5_str, SIZE_MD5_STR + 1);
	}
}

//Convierte el hash MD5 h a una cadena de caracteres str
void md5_bin2str(char *str, md5_hash_t h)
{
	char *byte_str = str;
	for(int i=0;i<16;i++) {
		sprintf(byte_str, "%2.2x", h.b[i]);
		byte_str += 2;
	}
	str[SIZE_MD5_STR] = '\n';
}

// Thread routine 
void *thread(void *vargp)
{
	int connfd = *((int *)vargp);
	Pthread_detach(pthread_self());
	Free(vargp);
	hash(connfd);
	Close(connfd);
	return NULL;
}

