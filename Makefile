CC = gcc
CFLAGS = -Wall -I .

# This flag includes the Pthreads library on a Linux box.
# Others systems will probably require something different.
LIB = -lpthread

.PHONY: all
all: client md5_server

client: client.c csapp.o
	$(CC) $(CFLAGS) -o client client.c csapp.o $(LIB)

md5_server: md5_server.c md5.c csapp.o md5.o
	$(CC) $(CFLAGS) -o md5_server md5_server.c csapp.o md5.o $(LIB)

csapp.o: csapp.c
	$(CC) $(CFLAGS) -c csapp.c

md5.o: md5.c
	$(CC) $(CFLAGS) -c md5.c

.PHONY: clean
clean:
	rm -f *.o client md5_server *~
