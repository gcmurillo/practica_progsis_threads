# Pr�ctica 13: Uso de threads con Pthread

Este es un repositorio esqueleto para la pr�ctica 13 de la materia Programaci�n de Sistemas (CCPG1008) P1 de la ESPOL.

### Uso ###
El repositorio contiene una aplicaci�n cliente - servidor. El servidor espera una cadena de caracteres del cliente, calcula el hash MD5 de la misma y retorna el hash en ASCII al cliente.

Ejecutar el cliente:

```
./client <host> <port>
```
Ejemplo:

```
./client 127.0.0.1 8080
```

Ejecutar el servidor:

```
./md5_server [-n] -p <port>
```

La opci�n -n es opcional y causa que el servidor ignore en el c�lculo del hash MD5 el salto de l�nea '\n' al final de la cadea de caracteres enviada por el cliente. La opci�n -p especifica el puerto.

Ejemplo:

```
./md5_server -n -p 8080
```

### �C�mo empiezo? ###

* Hacer un fork de este repositorio a su cuenta personal de Bitbucket (una cuenta por grupo)
* Clonar el repositorio en su cuenta (no esta) en su computadora del laboratorio
* Completar la pr�ctica en grupo
* Haga commit y push a su trabajo
* El entregable es un enlace al repositorio

### Integrantes ###

Edite esta lista y a�ada los nombres de los integrantes, luego borre esta l�nea.

* Geancarlo Murillo
* Luis Rodriguez
